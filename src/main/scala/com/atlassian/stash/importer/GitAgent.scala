package com.atlassian.stash.importer

import java.io.{FileFilter, File}

trait GitAgent {
  def setRemote(remoteName: String, remoteUrl: String, repoDir: File): Either[String, String]
  def push(remote: String, repoDir: File): Either[String, String]
  def findGitRepos(dirsToScan: Seq[File], lookInSubDirs: Boolean = true) : Either[Seq[PreImportError], Seq[File]] = {
    def findGitRepoChildren(dir: File): Seq[File] = {
      dir.listFiles(new FileFilter {
        def accept(child: File) = isReadableDir(child) && !child.getName.startsWith(".") && hasDotGitDir(child)
      }).foldLeft(Seq[File]()) { (gitDirs, child) => gitDirs :+ child }
    }

    val scanResult: Seq[Either[PreImportError, Seq[File]]] = for (dir <- dirsToScan) yield {
      if (!dir.exists()) {
        Left(PreImportError(dir, "Directory \"%s\" does not exist".format(dir)))
      } else if (!dir.isDirectory) {
        Left(PreImportError(dir, "\"%s\" is not a directory".format(dir)))
      } else if (!dir.canRead) {
        Left(PreImportError(dir, "Directory \"%s\" is not readable".format(dir)))
      } else if (isGitDir(dir)) {
        Right(Seq(dir))
      } else if (lookInSubDirs) {
        val repos = findGitRepoChildren(dir)
        if (repos.isEmpty) {
          Left(PreImportError(dir, "Directory \"%s\" is not a git repository and does not contain git repositories".format(dir)))
        } else {
          Right(repos)
        }
      } else {
        Left(PreImportError(dir, "Directory \"%s\" is not a git repository".format(dir)))
      }
    }

    Either.cond(
      scanResult.find(_.isRight).isDefined,        //Any errors?
      scanResult.map(_.right).map(_.get).flatten,  //If not then collate the repos
      scanResult.map(_.left).map(_.get))           //Otherwise collate the errors
  }

  protected def isGitDir(dir: File): Boolean = hasDotGitDir(dir)

  protected def isReadableDir(dotGit: File) = dotGit.exists() && dotGit.isDirectory && dotGit.canRead

  protected def hasDotGitDir(file: File): Boolean = {
    isReadableDir(new File(file, ".git"))
  }
}

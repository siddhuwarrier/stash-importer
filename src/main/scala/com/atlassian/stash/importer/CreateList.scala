package com.atlassian.stash.importer

import org.clapper.argot.ArgotConverters._
import java.io.{PrintWriter, FileWriter, File}
import collection.generic.CanBuildFrom

object CreateList extends ImportCommand {
  def name = "createlist" //must be def not val because needed before this class is initialised
  val description = "Detects git repositories in the local file system and generates an import list file for use with the importlist command."

  private val projKey = parser.option[String](List("k", "projkey"), "key", "The key of the project to import all repositories into. " +
    "If specified and the project doesn't already exist, you must also specify the projname argument. " +
    "If not specified then a new project is created for each repository.")
  private val projName = parser.option[String](List("n", "projname"), "name", "The name of the project to import all repositories into. " +
    "Only required if the projkey is specified and the project does not yet exist. Ignored otherwise.")
  private val listFile = parser.parameter[String]("listpath", "Path to a file where the list of repositories to import, their project keys and repository slugs will be written in CSV format.", optional = false)
  private val paths = parser.multiParameter[String]("dir", "Path to the repository to import or path to a directory containing repositories.", optional = false)

  override def run(git: GitAgent, stash: Stash, console: Logger) = {
    import console.{ log => conLog, logReturn => conLogReturn}

    val results: Either[Seq[PreImportError], Seq[ImportPending]] = scanDirectories(git, stash, chooseUniqueProjNameIfKeyNotExist(stash, console), console)

    results.fold(
      preImportErrors => {
        preImportErrors.foreach(error => conLogReturn("Directory \"%s\" can not be imported: %s.".format(error.dir, error.message)))
      },
      toImport => {
        val file = resolvePath(listFile.value.get)
        writeImportOutcomes(file, toImport)

        conLog("")
        conLog("Import list for %d %s was saved to \"%s\".".format(toImport.size, Pluralize.pluralize(toImport.size, "repository", "repositories"), file))
        conLog("Please manually inspect this file, amending any import entries as necessary, and then run the importlist command on this file to start the import.")
      })

    results.isRight
  }

  private def scanDirectories(git: GitAgent, stash: Stash, uniqueProjName: Option[String], consoleLogger: Logger): Either[Seq[PreImportError], Seq[ImportPending]] = {
    import consoleLogger.{ log => conLog }

    conLog("Looking for git repositories in %s.".format(paths.value.map("\"%s\"".format(_)).mkString(", ")))

    git.findGitRepos(paths.value.map(new File(_))).fold(
      error => Left(error),
      dirs => {
        val dirResults: Seq[Either[PreImportError, ImportPending]] = for (dir <- dirs) yield {
          conLog("Found git repository \"%s\".".format(dir.getAbsolutePath))
          val newProjKey = projKey.value.orElse(Some(dir.getName)).flatMap(stash.toLegalProjectKey(_)).getOrElse(stash.chooseProjectKey())
          val newProjName = uniqueProjName.orElse(Some(newProjKey)).flatMap(stash.toLegalProjectName(_)).getOrElse(stash.chooseProjectName())
          stash.reserveRepoName(newProjKey, dir.getName).fold(
            error => Left(PreImportError(dir, error)),
            newRepoName => Right(ImportPending(ImportStep(dir, newProjKey, newProjName, newRepoName)))
          )
        }

        //Split the Seq[Either[PreImportError, ImportPending]] into Either[Seq[PreImportError], Seq[ImportPending]]
        val errors: Seq[PreImportError] = for (Left(x) <- dirResults) yield(x)
        if (errors.isEmpty) Right(for (Right(x) <- dirResults) yield(x)) else Left(errors)
      })
  }

  private def chooseUniqueProjNameIfKeyNotExist(stash: Stash, consoleLogger: Logger): Option[String] = {
    import consoleLogger.{ log => conLog }

    //If a proj for the key exists, we ignore the proj name. If a proj for the key does not exist, make sure the proj name is unique
    if (projKey.value.isDefined && projName.value.isDefined && !stash.hasProjectWithKey(projKey.value.get) && stash.hasProjectWithName(projName.value.get)) {
      val chosen = stash.chooseUnusedProjectName(projName.value.get)
      conLog("The project name \"%s\" is already taken. Choosing \"%s\" instead." format (projName.value.get, chosen))
      Some(chosen)
    } else {
      projName.value
    }
  }
}

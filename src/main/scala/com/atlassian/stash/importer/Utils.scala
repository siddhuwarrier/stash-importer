package com.atlassian.stash.importer

import java.io.{StringWriter, File, PrintWriter}
import scala.Predef._
import java.util.Locale
import collection.generic.CanBuildFrom

trait Logger {
  def log(s: String = "", t: Option[Throwable] = None)

  def logReturn[A](s: String = "", t: Option[Throwable] = None): String = logWith(s, t)(s)

  def logWith[A](s: String = "", t: Option[Throwable] = None)(f: => A): A = {
    log(s, t)
    f
  }
}

class NoopLogger extends Logger {
  def log(s: String, t: Option[Throwable]) {}
}

class PrintLogger(val out: PrintWriter) extends Logger {
  def log(s: String, t: Option[Throwable]) {
    out.println(s)
    if (t.isDefined) {
      t.get.printStackTrace(out)
    }
    out.flush()
  }
}

class StringLogger() extends Logger {
  val writer = new PrintWriter(new StringWriter())

  def log(s: String, t: Option[Throwable]) {
    writer.write(s)
    if (t.isDefined) {
      t.get.printStackTrace(writer)
    }
    writer.flush()
  }

  def getOutput = writer.toString
}

class TeeLogger(val one: Logger, val two: Logger) extends Logger {
  def log(s: String, t: Option[Throwable]) {
    one.log(s, t)
    two.log(s, t)
  }
}

object Version {
  val version = Option.apply(Version.getClass.getPackage.getImplementationVersion).getOrElse("[dev build]")
}

object ImportState extends Enumeration {
  type ImportState = Value
  val Pending, Succeeded, Failed = Value
}
import ImportState._

case class ImportStep(dir: File, projKey: String, projName: String, repoName: String)

abstract class ImportOutcome {
  val step: ImportStep
  val state: ImportState
  val comment: String
}

case class ImportError(step: ImportStep, comment: String = "Import failed for unknown reason", e: Option[Exception] = None) extends ImportOutcome { val state: ImportState = Failed }
case class ImportSuccess(step: ImportStep, comment: String = "Import succeeded") extends ImportOutcome { val state: ImportState = Succeeded }
case class ImportPending(step: ImportStep, comment: String = "Not yet imported") extends ImportOutcome { val state: ImportState = Pending }
case class PreImportError(dir: File, message: String)
case class ImportLineError(line: Int, message: String, exception: Option[Exception] = None)

object RepoState extends Enumeration {
  type RepoState = Value
  val Unknown, Initialising, Available, InitialisationFailed = Value

  def fromStashName(s: String): Value = values.find(_.toString.toLowerCase(Locale.US) == s.toLowerCase(Locale.US).replaceAll("\\W", "")).get
}

object Pluralize {
  def pluralize(count: Int, noun: String, nouns: String) = if (count == 1) noun else nouns
}

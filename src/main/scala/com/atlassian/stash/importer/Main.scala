package com.atlassian.stash.importer

import java.io.{FileWriter, BufferedWriter, PrintWriter, File}
import org.clapper.argot.ArgotUsageException
import java.lang.String

object Main extends App {
  val commands = Array(CreateList, ImportList, ImportOne)

  object Help extends Command {
    val name = "help"
    val description = "Displays help information on stash-import"
    val usage = "Usage: \n\tstash-import --version\n\tstash-import --help [command]\n\tstash-import <command> [OPTIONS] <arguments>"

    override def printHelp() {
      println(usage)
      println()
      println("Supported commands are:")
      for (command <- commands) {
        println("\t%s\t%s" format(command.name, command.description))
      }
      println()
      println("See stash-import --help <command> for more information on a specific command.")
    }

    def apply(arguments: Array[String], fileLogger: Logger) = {
      println("Unrecognised or missing command.")
      println()
      println(usage)
      true
    }
  }

  if (args.length > 0 && args(0).toLowerCase == "--version") {
    println("stash-import version %s" format Version.version)
  } else {
    val (helpForCommand, realArgs) = args.partition(_.toLowerCase == "--help")
    val command = realArgs.headOption.flatMap(command => commands.find(_.name == command.toLowerCase)).getOrElse(Help)
    if (helpForCommand.nonEmpty) {
      command.printHelp()
    } else {
        val logfile = new File(System.getProperty("java.io.tmpdir"), "stash-import.log")
        val writer = new PrintWriter(new BufferedWriter(new FileWriter(logfile, true)))
        try {
          if (command(args.drop(1), new PrintLogger(writer))) {
            sys.exit(1)
          } else {
            writer.close()
            println("Logging information was written to \"%s\"" format (logfile))
            sys.exit(0)
          }
        } catch {
          case e: ArgotUsageException => {
            println(e.message)
            sys.exit(0)
          }
          case e: Exception => {
            e.printStackTrace(writer)
            println("An unexpected error occured, please attach the log file \"%s\" to any support issue you create" format (logfile))
            throw e
          }
        } finally {
          writer.close()
        }
    }
  }
}

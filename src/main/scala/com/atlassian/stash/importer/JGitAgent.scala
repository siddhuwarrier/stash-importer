package com.atlassian.stash.importer

import java.io.File
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.storage.file.FileRepository
import org.eclipse.jgit.transport.{PushResult, UsernamePasswordCredentialsProvider}
import org.eclipse.jgit.errors.RepositoryNotFoundException

class JGitAgent(username: String, password: String, logger: Logger) extends GitAgent {
  import logger._

  def setRemote(remoteName: String, remoteUrl: String, repoFile: File): Either[String, String] =
    try {
      val repo = new FileRepository(new File(repoFile, ".git"))
      val config = repo.getConfig
      config.setString("remote", remoteName, "url", remoteUrl)
      config.setString("remote", remoteName, "fetch", "+refs/heads/*:refs/remotes/%s/*".format(remoteName))
      config.save()

      Right(remoteName)
    } catch {
      case e: Exception => Left(logReturn("Error creating Git remote: %s".format(remoteUrl), Some(e)))
    }

  def push(remote: String, repoDir: File): Either[String, String] =
    try {
      Git.open(repoDir)
        .push()
        .setRemote(remote)
        .setPushAll()
        .setPushTags()
        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
        .call()
      Right("Successfully pushed to Stash.")
    } catch {
      case e: Exception => Left(logReturn("Error pushing remote: %s".format(e), Some(e)))
    }

  override protected def isGitDir(dir: File) =
    try {
      Git.open(dir)
      true
    } catch {
      case e: RepositoryNotFoundException => false
    }
}

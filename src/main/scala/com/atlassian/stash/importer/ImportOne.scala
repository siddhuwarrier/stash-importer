package com.atlassian.stash.importer

import org.clapper.argot.ArgotConverters._
import java.io.File
import ImportState._

object ImportOne extends ImportCommand {
  def name = "import" //must be def not val because needed before this class is initialised
  val description = "Imports a single git repository from the local filesystem into Stash."

  private val projKey = parser.option[String](List("k", "projkey"), "key", "The key of the project to import the repository into. " +
    "If specified and the project doesn't already exist, you must also specify the projname argument. " +
    "If not specified then a new project is created for each repository.")
  private val projName = parser.option[String](List("n", "projname"), "name", "The name of the project to import the repository into. " +
    "Only required if the projkey is specified and the project does not yet exist.")
  private val repoSlug = parser.option[String](List("s", "reposlug"), "slug", "The slug of the repository to import into. " +
    "If specified will be used instead of a calculated repository slug. " +
    "If not specified then the repository slug will be calculated from the repository directory name.")
  private val repoPath = parser.parameter[String]("repo", "Path to the repository to import.", optional = false)

  override def run(git: GitAgent, stash: Stash, console: Logger) : Boolean = {
    import console.{ log => conLog, logReturn => conLogReturn }

    val repoDir = resolvePath(repoPath.value.get)

    val importResult = importRepo(repoDir, projKey.value, projName.value, repoSlug.value, git, stash, console)

    importResult.fold(
      (error) => {
        conLog("Importing of \"%s\" could not be started: %s".format(error.dir, error.message))
      },
      (outcome) => {
        outcome.state match {
          case Succeeded => conLogReturn("Importing \"%s\" succeeded: %s".format(outcome.step.dir, outcome.comment))
          case Failed =>    conLogReturn("Importing \"%s\" failed: %s".format(outcome.step.dir, outcome.comment))
          case Pending =>     conLogReturn("Importing \"%s\" was skipped: %s".format(outcome.step.dir, outcome.comment))
        }
      }
    )

    importResult.isRight
  }

  protected def importRepo(dir: File, projKey: Option[String], projName: Option[String], repoSlug: Option[String], git: GitAgent, stash: Stash, console: Logger): Either[PreImportError, ImportOutcome] = {
    git.findGitRepos(Seq(dir), lookInSubDirs = false).fold(
      errors => Left(errors.head),
      dirs => Right({
        val legalProjectKey: String = projKey.orElse(stash.toLegalProjectKey(dir.getName)).getOrElse(stash.chooseProjectKey())
        importRepo(
          ImportPending(
            ImportStep(
              dir,
              legalProjectKey,
              projName.getOrElse(stash.toLegalProjectName(dir.getName).getOrElse(stash.chooseProjectName())),
              repoSlug.orElse(stash.toLegalRepoName(dir.getName)).getOrElse(stash.chooseRepoName(legalProjectKey)))),
          git, stash, console)
      }))
  }
}

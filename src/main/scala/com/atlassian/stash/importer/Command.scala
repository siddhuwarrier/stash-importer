package com.atlassian.stash.importer

import org.clapper.argot.ArgotParser

trait Command {
  def name: String
  def description: String
  def printHelp() {
    println(parser.usageString(Some("Description: %s".format(description))))
  }

  protected lazy val parser = new ArgotParser("stash-importer %s" format name)

  def apply(args: Array[String], fileLogger: Logger): Boolean
}

@echo off

if "%JAVA_HOME%" == "" goto javaHomeNotSet

echo Using JAVA_HOME:       "%JAVA_HOME%"

set PRGDIR=%~dp0
set JAVA_OPTS=-Xmx1024m -XX:MaxPermSize=256m -Djava.awt.headless=true


"%JAVA_HOME%\bin\java" %JAVA_OPTS% -classpath "%PRGDIR%\..\lib\stash-importer.jar" com.atlassian.stash.importer.Main %*
goto end

:javaHomeNotSet
echo You must set the JAVA_HOME environment variable to run this program

:end
#!/bin/sh -e

# resolve links - $0 may be a softlink
PRG="$0"
while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done
PRGDIR=`dirname "$PRG"`

export JAVA_OPTS="-Xmx2048m -XX:MaxPermSize=512m -Djava.awt.headless=true"

$JAVA_HOME/bin/java $JAVA_OPTS -classpath $PRGDIR/../lib/stash-importer.jar com.atlassian.stash.importer.Main "$@"